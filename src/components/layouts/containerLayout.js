import Head from 'next/head';

import Header from '../molecules/Header/Header';

import styles from './containerLayout.module.scss';

export default function containerLayout({ children, siteData }) {
  return (
    <div className={styles.container}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="author" content={siteData.author} />
        <meta name="description" content={siteData.description} />
        <title>{siteData.title}</title>
        <meta property="og:title" content={siteData.title} key="title" />
      </Head>
      <Header />
      <main className={styles.main}>{children}</main>
    </div>
  );
}
