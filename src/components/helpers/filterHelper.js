/**
 * @param {Array.<Object>} allItems - array which will be filtered using filter template
 * @param {Object} filterTemplate - the template used for filtering the array
 * @returns
 */
export function filterHelper(allItems, filterTemplate) {
  return allItems.filter((item) => {
    return Object.keys(filterTemplate).every(
      (propertyName) => item[propertyName] === filterTemplate[propertyName]
    );
  });
}
