/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect, useCallback } from 'react';
import { useRouter } from 'next/router';
import menuItems from '../../../assets/data/menuItems';

import MainMenu from '../Menus/Main';
import DeptLogo from '../../../assets/icons/icon-dept.js';
import Image from '../../atoms/Images/Image';

import styles from './Header.module.scss';
import AnchorTag from '../../atoms/Links/AnchorTag';

/** Main header containing logo, hero and menu */
export default function Header() {
  const [isExpanded, setIsExpanded] = useState(false);
  const { pathname } = useRouter();
  const [pageTitle, setPageTitle] = useState('');

  // Close menu when menu item is selected
  useEffect(() => {
    if (isExpanded) toggleMenu();
    console.log();
    setPageTitle(
      menuItems.menuItems.find((i) => pathname.includes(i.link))?.link
    );
  }, [pathname]);

  // Make sure the escape key closes the menu
  useEffect(() => {
    document.addEventListener('keydown', escFunction);

    return () => {
      document.removeEventListener('keydown', escFunction);
    };
  }, []);

  const escFunction = useCallback((event) => {
    if (event.keyCode === 27 && isExpanded) {
      toggleMenu();
    }
  }, []);

  const toggleMenu = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <header className={styles.header} data-is-expanded={isExpanded}>
      <div className={styles.nav}>
        <div className={styles.logo}>
          <AnchorTag href="/">
            <DeptLogo />
            <span className="screenreader">Home</span>
          </AnchorTag>
        </div>
        <MainMenu isExpanded={isExpanded} toggleMenu={toggleMenu} />
      </div>
      {!isExpanded && (
        <div className={styles.hero}>
          <Image src={`images/Header.webp`} alt="" layout="responsive" />
          <h1 className={styles.hero__title}>{pageTitle}</h1>
        </div>
      )}
    </header>
  );
}
