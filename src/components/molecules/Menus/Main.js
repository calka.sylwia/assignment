import { useRef, useEffect } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import menuItems from '../../../assets/data/menuItems';

import MenuIcon from '../../../assets/icons/icon-menu.js';
import CloseIcon from '../../../assets/icons/icon-close.js';
import Navigation from './Navigation';

import styles from './Main.module.scss';

const MainMenu = ({ isExpanded, toggleMenu }) => {
  const closeButton = useRef();
  const openButton = useRef();
  const { pathname } = useRouter();

  // Switch focus between menu and close button
  useEffect(() => {
    isExpanded ? closeButton.current.focus() : openButton.current.focus();
  }, [isExpanded]);

  return (
    <div>
      <button
        onClick={toggleMenu}
        aria-expanded={isExpanded}
        aria-controls="menu-list"
        hidden={isExpanded}
        ref={openButton}
        className={styles.button__menu}
      >
        <span>Menu</span>
        <MenuIcon />
      </button>
      <div className={styles['menu--expanded']} hidden={!isExpanded}>
        <button
          onClick={toggleMenu}
          type="button"
          id="close"
          aria-label="Close menu"
          ref={closeButton}
          className={styles.button__close}
        >
          <CloseIcon />
        </button>
        <Navigation
          id="language-nav"
          menuTitle="Landen"
          showTitle
          customClass={styles.menu__languages}
        >
          {menuItems.locales.map((item) => (
            <Navigation.Item
              key={item.title}
              title={item.title}
              link={item.link}
              isActive={pathname.includes(item.link)}
            />
          ))}
        </Navigation>
        <Navigation
          id="primary-nav"
          menuTitle="Primary navigation"
          isLarge
          hasBorded
          customClass={styles.menu__primary}
        >
          {menuItems.menuItems.map((item) => (
            <Navigation.Item
              key={item.link}
              title={item.title}
              link={item.link}
              isActive={pathname.includes(item.link)}
            />
          ))}
        </Navigation>
      </div>
    </div>
  );
};

const { func, bool } = PropTypes;

MainMenu.propTypes = {
  isExpanded: bool,
  toggleMenu: func,
};

export default MainMenu;
