import PropTypes from 'prop-types';
import classNames from 'classnames';

import AnchorTag from '../../atoms/Links/AnchorTag';
import IconDropdown from '../../../assets/icons/icon-dropdown.js';

import styles from './Navigation.module.scss';

/**
 * Navigation component
 */
const Navigation = ({
  menuTitle = '',
  showTitle = false,
  id = '',
  isLarge = false,
  hasBorded = false,
  children,
  customClass = '',
}) => {
  const classes = classNames(styles.nav, customClass, {
    [styles['nav--large']]: isLarge,
    [styles['nav--small']]: !isLarge,
    [styles['nav--border']]: hasBorded,
  });

  return (
    <nav className={classes} aria-labelledby={id}>
      <h2 id={id} className={`info-text ${showTitle ? '' : 'screenreader'}`}>
        {menuTitle}
      </h2>
      <ol className={styles.nav__list}>{children}</ol>
    </nav>
  );
};

const Item = ({ title = '', link = '/', isActive = false }) => {
  return (
    <li key={title} data-active={isActive}>
      {isActive && (
        <span className={styles.nav__icon}>
          <IconDropdown />
        </span>
      )}
      <AnchorTag href={link}>{title}</AnchorTag>
    </li>
  );
};

Navigation.Item = Item;

const { string, bool, node } = PropTypes;

Navigation.propTypes = {
  menuTitle: string,
  showTitle: bool,
  id: string,
  isLarge: bool,
  hasBorded: bool,
  children: node,
  customClass: string,
};

export default Navigation;
