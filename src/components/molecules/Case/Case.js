import PropTypes from 'prop-types';
import classNames from 'classnames';

import Image from '../../atoms/Images/Image';
import AnchorTag from '../../atoms/Links/AnchorTag';
import ReadMore from '../../atoms/Links/ReadMore';

import styles from './Case.module.scss';

/**
 * Shows case info
 */
const Case = ({ title = '', category = '', link, imgSrc, imgAlt = '' }) => {
  const classes = classNames(
    styles.case,
    imgSrc ? styles['case--primary'] : styles['case--secondary']
  );

  return (
    <li className={classes}>
      <article>
        <div>
          {imgSrc && (
            <Image src={`images/${imgSrc}`} alt={imgAlt} layout="responsive" />
          )}
        </div>
        <div className="info-text">{category}</div>
        <h2>
          <AnchorTag customClass={styles.title} href={link}>
            {title}
          </AnchorTag>
        </h2>
        <div className={styles.readmore}>
          <ReadMore customClass={styles.readmore} href={link} isLink={false}>
            View case
          </ReadMore>
        </div>
      </article>
    </li>
  );
};

/** Blockquote case */
const Quote = ({ quote, author }) => {
  return (
    <li className={styles.case__quote}>
      <figure>
        <blockquote>
          <p className="h2">{quote}</p>
        </blockquote>
        <figcaption className="info-text">{author}</figcaption>
      </figure>
    </li>
  );
};

/** Grid container for cases showcase */
const CaseContainer = ({ children, displayType }) => {
  return (
    <section>
      <h2 className="screenreader">Cases</h2>
      {!children.length ? (
        <h2>No cases match your search criteria</h2>
      ) : (
        <ul data-display={displayType} className={styles.container}>
          {children}
        </ul>
      )}
    </section>
  );
};

Case.Quote = Quote;
Case.CaseContainer = CaseContainer;

const { string } = PropTypes;

Case.propTypes = {
  title: string.isRequired,
  category: string.isRequired,
  link: string,
  /** Determines if the image is shown and otherwise smaller version of case is displayed */
  imgSrc: string,
  imgAlt: string,
};

export default Case;
