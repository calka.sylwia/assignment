import NextImage from 'next/image';
import PropTypes from 'prop-types';

import mockBlurDataURL from './mockBlurUrl';

/**
 * With next/image images are optimised automatically by default. This optimisation allows for resizing, optimizin and serving images in formats like WebP. With direct import of images from the project, width, height and blurDataURL are automatically provided. For dynamic or remote images, provide w,h, and blurDataURL
 */
const Image = ({
  src,
  alt,
  width = 600,
  height = 460,
  layout = 'intrinsic',
  placeholder = 'blur',
  blurDataURL,
}) => {
  if (src.includes('?')) {
    const dimensions = src.split('?');
    const widthHeight = dimensions[1].split('x');
    width = widthHeight[0];
    height = widthHeight[1];
  }
  const srcPath = `/${src}`;

  return (
    <NextImage
      loading="lazy"
      src={srcPath}
      alt={alt}
      width={width}
      height={height}
      layout={layout}
      placeholder={placeholder}
      blurDataURL={mockBlurDataURL.url}
    />
  );
};

const { string, number, oneOf } = PropTypes;

Image.propTypes = {
  src: string,
  alt: string,
  width: number,
  height: number,
  layout: oneOf(['intrinsic', 'responsive', 'fixed', 'fill']),
  placeholder: oneOf(['blur', 'empty']),
  /** blur data should be generate in backend for each image */
  blurDataURL: string,
};

export default Image;
