import PropTypes from 'prop-types';

import styles from './Filter.module.scss';

/**
 * Filter components
 */
const Filters = ({ id, label, filters, selectedFilters, handleFilter }) => {
  return (
    <div className={styles.filter}>
      <label className="h2 grey" htmlFor={id}>
        {label}
      </label>

      <select
        className={`h2 medium-bold ${styles.select}`}
        id={id}
        value={selectedFilters[id]}
        onChange={handleFilter}
      >
        {filters.map((filter) => (
          <option className="h2" key={filter.id} value={filter.id}>
            {filter.label}
          </option>
        ))}
      </select>
    </div>
  );
};

const FilterContainer = ({ children }) => {
  return <div className={styles.container}>{children}</div>;
};

Filters.Container = FilterContainer;

const { func, string, array, shape } = PropTypes;

Filters.propTypes = {
  id: string /** id of the filter and select element */,
  label: string,
  filters: array,
  selectedFilters: shape({
    id: string,
  }),
  handleFilter: func,
};

export default Filters;
