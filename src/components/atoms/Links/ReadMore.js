import PropTypes from 'prop-types';

import IconDropdown from '../../../assets/icons/icon-dropdown.js';
import AnchorTag from './AnchorTag';

import styles from './ReadMore.module.scss';

/**
 * View more link
 */
const ReadMore = ({ children, href = '/', isLink = true }) => {
  return (
    <AnchorTag
      href={href}
      aria-hidden="true"
      customClass={styles.readmore}
      tabIndex={isLink ? 0 : -1}
    >
      <IconDropdown />
      {children}
    </AnchorTag>
  );
};

const { node, string, bool } = PropTypes;

ReadMore.propTypes = {
  children: node,
  href: string,
  /** Switch betwen normal anchor tag and only-visual anchor tag that is not focusable */
  isLink: bool,
};

export default ReadMore;
