import PropTypes from 'prop-types';

import Link from 'next/link';

/** Basic anchor tag component */
const AnchorTag = ({ href, title, children, customClass, ...attributes }) => {
  return (
    <Link href={href}>
      <a href={href} className={customClass} {...attributes}>
        {title || children}
      </a>
    </Link>
  );
};

const { node, string } = PropTypes;

AnchorTag.propTypes = {
  title: string,
  children: node,
  href: string.isRequired,
  customClass: string,
};

export default AnchorTag;
