const IconDropdown = ({ rotate = 0 }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 36 36"
    aria-hidden="true"
    focusable="false"
    fill="currentColor"
    transform={`rotate(${rotate})`}
  >
    <g fill="none" fillRule="evenodd">
      <path d="M0 0h36v36H0z" />
      <path fill="currentColor" d="m5 36 28-18.019L5 0z" />
    </g>
  </svg>
);

export default IconDropdown;
