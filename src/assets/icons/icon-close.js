const CloseIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 16 16"
    width="16"
    height="16"
    aria-hidden="true"
    focusable="false"
    fill="currentColor"
  >
    <g fill="currentColot" fillRule="evenodd">
      <path d="M1.636 0l14.142 14.142-1.414 1.414L.222 1.414z" />
      <path d="M.222 14.364L14.364.222l1.414 1.414L1.636 15.778z" />
    </g>
  </svg>
);

export default CloseIcon;
