const MenuIcon = () => (
  <svg
    width="20"
    height="9"
    xmlns="http://www.w3.org/2000/svg"
    aria-hidden="true"
    focusable="false"
  >
    <g fillRule="evenodd">
      <path d="M0 7h20v2H0zM0 0h20v2H0z" />
    </g>
  </svg>
);

export default MenuIcon;
