# Dept assignment - Sylwia Calka

This is a Frontend Developer assignment for Dept. It is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app). It uses static  site generation (SSG) to generate static HTML and JSON.
## Getting Started
1. Clone this repo
2. Install dependencies `yarn install`
3. Run `yarn dev`
4. Open [http://localhost:3000](http://localhost:3000) with your browser to see the result
5. or check production build code in `.next` folder and run it with `yarn start`. If this is not going to start, re-build with `yarn build`

## Main features
### Pages
- routing between pages (working pages are: /, /work, /industry, /services)
- /work page showcasing the client cases
- accessible menu navigation, can be navigated by keyboard

### Cases
- semi-dynamic layout based on data
- filtering by category or industry
- grid and list view


### Other
- WebP images
- Blur image on load (used one placeholder, but in production the image should come from backend)


## Notes
- Used NextJs with SCSS 
- Versions: Node v14.17.5, Npm 6.14.14