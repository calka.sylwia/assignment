import AnchorTag from '../src/components/atoms/Links/AnchorTag';

/** Home page */
export default function Home() {
  return (
    <h1>
      Hi, please go to <AnchorTag href="/work">work</AnchorTag> page
    </h1>
  );
}
