import siteData from '../siteconfig';

import ContainerLayout from '../src/components/layouts/containerLayout';

import '../src/styles/all.scss';

function MyApp({ Component, pageProps }) {
  return (
    <ContainerLayout siteData={siteData}>
      <Component {...pageProps} />
    </ContainerLayout>
  );
}

export default MyApp;
