import { useState, useEffect } from 'react';
import { getMockCases } from '../lib/getMockCases';
import { filterHelper } from '../src/components/helpers/filterHelper';

import Filter from '../src/components/atoms/Filters/Filter';
import Case from '../src/components/molecules/Case/Case';

/**
 * Page containing all work cases with filter functionality
 */
const Work = ({ mockCases }) => {
  const [filters, setFilters] = useState({});
  const [cases, setCases] = useState([]);
  const [displayType, setDisplayType] = useState({ id: 'grid' });

  useEffect(() => {
    setCases(mockCases.cases);
  }, [mockCases]);

  useEffect(() => {
    // Filter cases based on selected filters
    const filteredCases = filterHelper(mockCases.cases, filters);
    setCases(filteredCases);
  }, [filters, mockCases]);

  const handleFilter = ({ target: { id, value } }) => {
    // Remove filter
    if (value === 'all') {
      const { [id]: value, ...filtersRest } = filters;
      return setFilters(filtersRest);
    }
    // Add new filter
    return setFilters({ ...filters, [id]: value });
  };

  const handleDisplayType = ({ target: { value } }) => {
    setDisplayType({ id: value });
  };

  return (
    <>
      <Filter.Container>
        <Filter
          id="category"
          label={mockCases.categoryFiltersLabel}
          filters={mockCases.categoryFilters}
          selectedFilters={filters}
          handleFilter={handleFilter}
        />
        <Filter
          id="industry"
          label={mockCases.industryFiltersLabel}
          filters={mockCases.industryFilters}
          selectedFilters={filters}
          handleFilter={handleFilter}
        />
        <Filter
          id="displayType"
          label="View as"
          filters={[
            { label: 'grid', id: 'grid' },
            { label: 'list', id: 'list' },
          ]}
          selectedFilters={{ id: displayType.id }}
          handleFilter={handleDisplayType}
        />
      </Filter.Container>
      <Case.CaseContainer displayType={displayType.id}>
        {cases.map(
          ({ title, brand, imgSrc, id, type = '', imgAlt, author = '' }) =>
            type === 'quote' ? (
              <Case.Quote key={id} quote={title} author={author} />
            ) : (
              <Case
                key={id}
                title={title}
                category={brand}
                imgSrc={imgSrc}
                imgAlt={imgAlt}
                link={id}
                type={type}
                width={displayType.id === 'list' && 700}
              />
            )
        )}
      </Case.CaseContainer>
    </>
  );
};

// Fetch data at build
export async function getStaticProps() {
  // Get external data from the file system, API, DB, etc.
  const { mockCases } = getMockCases();
  return {
    props: {
      mockCases,
    },
  };
}

export default Work;
