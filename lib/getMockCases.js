import mockData from '../src/assets/data/mockData.json';

/**
 * Function to get mock data about work cases
 */
export function getMockCases() {
  const mockCases = JSON.parse(JSON.stringify(mockData));
  return { mockCases };
}
