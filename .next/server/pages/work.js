(() => {
var exports = {};
exports.id = 337;
exports.ids = [337];
exports.modules = {

/***/ 5806:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ work),
  "getStaticProps": () => (/* binding */ getStaticProps)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
;// CONCATENATED MODULE: ./src/assets/data/mockData.json
const mockData_namespaceObject = JSON.parse('{"cases":[{"title":"A summer island in the Netherlands","brand":"Bol.com","category":"technology","industry":"banks","imgSrc":"bolcom.webp","imgAlt":"Summer at bol.com","id":"bol"},{"title":"Not some average banking website","brand":"Kempen","category":"technology","industry":"education","imgSrc":"kempen.webp","imgAlt":"Banking website","id":"kempen"},{"title":"Beautiful design meets innovative technology","brand":"Philips","category":"design","industry":"fashion","imgSrc":"philips.webp","imgAlt":"Innovative technology","id":"philips"},{"title":"A 100 years of Mondriaan & De Stijl","brand":"Gemeentemuseum","industry":"fashion","imgSrc":"gemeentemuseum.webp","imgAlt":"Mondriaan & De Stijl","id":"gemeentemuseum"},{"title":"Tapping into Ireland’s unique gaming culture and bringing a fresh flavour to their Xbox social media channels","brand":"MICROSOFT","category":"technology","industry":"education","id":"microsoft"},{"title":"Integrating existing content into O’Neill’s new e-commerce platform","brand":"O’NEILL","category":"advertising","industry":"fashion","id":"oneill"},{"title":"A summer island in the Netherlands","brand":"Bol.com","category":"advertising","industry":"banks","imgSrc":"bolcom.webp","imgAlt":"Summer at bol.com","id":"bol2"},{"title":"Not some average banking website","brand":"Kempen","category":"advertising","industry":"education","imgSrc":"kempen.webp","imgAlt":"Banking website","id":"kempen2"},{"title":"Beautiful design meets innovative technology","brand":"Philips","category":"design","industry":"fashion","imgSrc":"philips.webp","imgAlt":"Innovative technology","id":"philips2"},{"title":"A 100 years of Mondriaan & De Stijl","brand":"Gemeentemuseum","category":"advertising","industry":"fashion","imgSrc":"gemeentemuseum.webp","imgAlt":"Mondriaan & De Stijl","id":"gemeentemuseum2"},{"title":"Innovative SEO and content strategy for Zalando","brand":"Zalando","category":"advertising","industry":"fashion","imgSrc":"zalando.webp","imgAlt":"Banking website","id":"zalando"},{"title":"The search for the most influential book ever","brand":"Koninklijke Bibliotheek","category":"technology","industry":"education","imgSrc":"koninklijke-bibliotheek.webp","imgAlt":"Banking website","id":"bibliotheek"},{"title":"“Dept helped us tell our story through a bold new identity and a robust online experience. To the tune of 60% growth in online bookings in just one month.”","author":"Mattijs Ten Brink - CEO, transavia","category":"design","industry":"technology","id":"quote","type":"quote"},{"title":"Not some average banking website","brand":"Kempen","category":"design","industry":"education","imgSrc":"kempen.webp","imgAlt":"Banking website","id":"kempen3"},{"title":"Beautiful design meets innovative technology","brand":"Philips","category":"technology","industry":"fashion","imgSrc":"philips.webp","imgAlt":"Innovative technology","id":"philips3"}],"categoryFiltersLabel":"Show me","categoryFilters":[{"id":"all","label":"all work"},{"id":"advertising","label":"Advertising"},{"id":"technology","label":"Technology"},{"id":"design","label":"Design"}],"industryFiltersLabel":"in","industryFilters":[{"id":"all","label":"all industries"},{"id":"education","label":"Education"},{"id":"fashion","label":"Fashion"},{"id":"banks","label":"Banks"}]}');
;// CONCATENATED MODULE: ./lib/getMockCases.js

/**
 * Function to get mock data about work cases
 */

function getMockCases() {
  const mockCases = JSON.parse(JSON.stringify(mockData_namespaceObject));
  return {
    mockCases
  };
}
;// CONCATENATED MODULE: ./src/components/helpers/filterHelper.js
/**
 * @param {Array.<Object>} allItems - array which will be filtered using filter template
 * @param {Object} filterTemplate - the template used for filtering the array
 * @returns
 */
function filterHelper(allItems, filterTemplate) {
  return allItems.filter(item => {
    return Object.keys(filterTemplate).every(propertyName => item[propertyName] === filterTemplate[propertyName]);
  });
}
// EXTERNAL MODULE: external "prop-types"
var external_prop_types_ = __webpack_require__(4229);
var external_prop_types_default = /*#__PURE__*/__webpack_require__.n(external_prop_types_);
// EXTERNAL MODULE: ./src/components/atoms/Filters/Filter.module.scss
var Filter_module = __webpack_require__(842);
var Filter_module_default = /*#__PURE__*/__webpack_require__.n(Filter_module);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./src/components/atoms/Filters/Filter.js


/**
 * Filter components
 */




const Filters = ({
  id,
  label,
  filters,
  selectedFilters,
  handleFilter
}) => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: (Filter_module_default()).filter,
    children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
      className: "h2 grey",
      htmlFor: id,
      children: label
    }), /*#__PURE__*/jsx_runtime_.jsx("select", {
      className: `h2 medium-bold ${(Filter_module_default()).select}`,
      id: id,
      value: selectedFilters[id],
      onChange: handleFilter,
      children: filters.map(filter => /*#__PURE__*/jsx_runtime_.jsx("option", {
        className: "h2",
        value: filter.id,
        children: filter.label
      }, filter.id))
    })]
  });
};

const FilterContainer = ({
  children
}) => {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: (Filter_module_default()).container,
    children: children
  });
};

Filters.Container = FilterContainer;
const {
  func,
  string,
  array,
  shape
} = (external_prop_types_default());
/* harmony default export */ const Filter = (Filters);
// EXTERNAL MODULE: external "classnames"
var external_classnames_ = __webpack_require__(4058);
var external_classnames_default = /*#__PURE__*/__webpack_require__.n(external_classnames_);
// EXTERNAL MODULE: ./src/components/atoms/Images/Image.js + 1 modules
var Image = __webpack_require__(8334);
// EXTERNAL MODULE: ./src/components/atoms/Links/AnchorTag.js
var AnchorTag = __webpack_require__(8478);
// EXTERNAL MODULE: ./src/assets/icons/icon-dropdown.js
var icon_dropdown = __webpack_require__(6464);
// EXTERNAL MODULE: ./src/components/atoms/Links/ReadMore.module.scss
var ReadMore_module = __webpack_require__(491);
var ReadMore_module_default = /*#__PURE__*/__webpack_require__.n(ReadMore_module);
;// CONCATENATED MODULE: ./src/components/atoms/Links/ReadMore.js




/**
 * View more link
 */




const ReadMore = ({
  children,
  href = '/',
  isLink = true
}) => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(AnchorTag/* default */.Z, {
    href: href,
    "aria-hidden": "true",
    customClass: (ReadMore_module_default()).readmore,
    tabIndex: isLink ? 0 : -1,
    children: [/*#__PURE__*/jsx_runtime_.jsx(icon_dropdown/* default */.Z, {}), children]
  });
};

const {
  node,
  string: ReadMore_string,
  bool
} = (external_prop_types_default());
/* harmony default export */ const Links_ReadMore = (ReadMore);
// EXTERNAL MODULE: ./src/components/molecules/Case/Case.module.scss
var Case_module = __webpack_require__(2051);
var Case_module_default = /*#__PURE__*/__webpack_require__.n(Case_module);
;// CONCATENATED MODULE: ./src/components/molecules/Case/Case.js






/**
 * Shows case info
 */




const Case = ({
  title = '',
  category = '',
  link,
  imgSrc,
  imgAlt = ''
}) => {
  const classes = external_classnames_default()((Case_module_default()).case, imgSrc ? (Case_module_default())["case--primary"] : (Case_module_default())["case--secondary"]);
  return /*#__PURE__*/jsx_runtime_.jsx("li", {
    className: classes,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("article", {
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        children: imgSrc && /*#__PURE__*/jsx_runtime_.jsx(Image/* default */.Z, {
          src: `images/${imgSrc}`,
          alt: imgAlt,
          layout: "responsive"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "info-text",
        children: category
      }), /*#__PURE__*/jsx_runtime_.jsx("h2", {
        children: /*#__PURE__*/jsx_runtime_.jsx(AnchorTag/* default */.Z, {
          customClass: (Case_module_default()).title,
          href: link,
          children: title
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (Case_module_default()).readmore,
        children: /*#__PURE__*/jsx_runtime_.jsx(Links_ReadMore, {
          customClass: (Case_module_default()).readmore,
          href: link,
          isLink: false,
          children: "View case"
        })
      })]
    })
  });
};
/** Blockquote case */


const Quote = ({
  quote,
  author
}) => {
  return /*#__PURE__*/jsx_runtime_.jsx("li", {
    className: (Case_module_default()).case__quote,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("figure", {
      children: [/*#__PURE__*/jsx_runtime_.jsx("blockquote", {
        children: /*#__PURE__*/jsx_runtime_.jsx("p", {
          className: "h2",
          children: quote
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("figcaption", {
        className: "info-text",
        children: author
      })]
    })
  });
};
/** Grid container for cases showcase */


const CaseContainer = ({
  children,
  displayType
}) => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("section", {
    children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
      className: "screenreader",
      children: "Cases"
    }), !children.length ? /*#__PURE__*/jsx_runtime_.jsx("h2", {
      children: "No cases match your search criteria"
    }) : /*#__PURE__*/jsx_runtime_.jsx("ul", {
      "data-display": displayType,
      className: (Case_module_default()).container,
      children: children
    })]
  });
};

Case.Quote = Quote;
Case.CaseContainer = CaseContainer;
const {
  string: Case_string
} = (external_prop_types_default());
/* harmony default export */ const Case_Case = (Case);
;// CONCATENATED MODULE: ./pages/work.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }

function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }






/**
 * Page containing all work cases with filter functionality
 */





const Work = ({
  mockCases
}) => {
  const {
    0: filters,
    1: setFilters
  } = (0,external_react_.useState)({});
  const {
    0: cases,
    1: setCases
  } = (0,external_react_.useState)([]);
  const {
    0: displayType,
    1: setDisplayType
  } = (0,external_react_.useState)({
    id: 'grid'
  });
  (0,external_react_.useEffect)(() => {
    setCases(mockCases.cases);
  }, [mockCases]);
  (0,external_react_.useEffect)(() => {
    // Filter cases based on selected filters
    const filteredCases = filterHelper(mockCases.cases, filters);
    setCases(filteredCases);
  }, [filters, mockCases]);

  const handleFilter = ({
    target: {
      id,
      value
    }
  }) => {
    // Remove filter
    if (value === 'all') {
      const {
        [id]: value
      } = filters,
            filtersRest = _objectWithoutProperties(filters, [id].map(_toPropertyKey));

      return setFilters(filtersRest);
    } // Add new filter


    return setFilters(_objectSpread(_objectSpread({}, filters), {}, {
      [id]: value
    }));
  };

  const handleDisplayType = ({
    target: {
      value
    }
  }) => {
    setDisplayType({
      id: value
    });
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Filter.Container, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(Filter, {
        id: "category",
        label: mockCases.categoryFiltersLabel,
        filters: mockCases.categoryFilters,
        selectedFilters: filters,
        handleFilter: handleFilter
      }), /*#__PURE__*/jsx_runtime_.jsx(Filter, {
        id: "industry",
        label: mockCases.industryFiltersLabel,
        filters: mockCases.industryFilters,
        selectedFilters: filters,
        handleFilter: handleFilter
      }), /*#__PURE__*/jsx_runtime_.jsx(Filter, {
        id: "displayType",
        label: "View as",
        filters: [{
          label: 'grid',
          id: 'grid'
        }, {
          label: 'list',
          id: 'list'
        }],
        selectedFilters: {
          id: displayType.id
        },
        handleFilter: handleDisplayType
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(Case_Case.CaseContainer, {
      displayType: displayType.id,
      children: cases.map(({
        title,
        brand,
        imgSrc,
        id,
        type = '',
        imgAlt,
        author = ''
      }) => type === 'quote' ? /*#__PURE__*/jsx_runtime_.jsx(Case_Case.Quote, {
        quote: title,
        author: author
      }, id) : /*#__PURE__*/jsx_runtime_.jsx(Case_Case, {
        title: title,
        category: brand,
        imgSrc: imgSrc,
        imgAlt: imgAlt,
        link: id,
        type: type,
        width: displayType.id === 'list' && 700
      }, id))
    })]
  });
}; // Fetch data at build


async function getStaticProps() {
  // Get external data from the file system, API, DB, etc.
  const {
    mockCases
  } = getMockCases();
  return {
    props: {
      mockCases
    }
  };
}
/* harmony default export */ const work = (Work);

/***/ }),

/***/ 842:
/***/ ((module) => {

// Exports
module.exports = {
	"filter": "Filter_filter__1m9P0",
	"select": "Filter_select__1-GIX",
	"container": "Filter_container__1LC4m"
};


/***/ }),

/***/ 491:
/***/ ((module) => {

// Exports
module.exports = {
	"readmore": "ReadMore_readmore__3OWUE"
};


/***/ }),

/***/ 2051:
/***/ ((module) => {

// Exports
module.exports = {
	"case": "Case_case__1MGXt",
	"title": "Case_title__sEIWm",
	"container": "Case_container__30UsP",
	"case--secondary": "Case_case--secondary__1-BXm",
	"case__quote": "Case_case__quote__1dMBS",
	"readmore": "Case_readmore__1pj4v",
	"case--primary": "Case_case--primary__1MNGS"
};


/***/ }),

/***/ 4058:
/***/ ((module) => {

"use strict";
module.exports = require("classnames");

/***/ }),

/***/ 9325:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 822:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 6695:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 8300:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/detect-domain-locale.js");

/***/ }),

/***/ 5378:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 7162:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 8773:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 2248:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 9372:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 665:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 2747:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 333:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 3456:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 556:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 7620:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 4229:
/***/ ((module) => {

"use strict";
module.exports = require("prop-types");

/***/ }),

/***/ 9297:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 5282:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [664,675,519,921], () => (__webpack_exec__(5806)));
module.exports = __webpack_exports__;

})();