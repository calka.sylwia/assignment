(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 2391:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

;// CONCATENATED MODULE: ./siteconfig.js
const config = {
  title: 'Sylwia Calka assignment',
  description: 'Sylwia Calka assignment',
  author: 'Sylwia Calka'
};
/* harmony default export */ const siteconfig = (config);
;// CONCATENATED MODULE: external "next/head"
const head_namespaceObject = require("next/head");
var head_default = /*#__PURE__*/__webpack_require__.n(head_namespaceObject);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
;// CONCATENATED MODULE: external "next/router"
const router_namespaceObject = require("next/router");
;// CONCATENATED MODULE: ./src/assets/data/menuItems.json
const menuItems_namespaceObject = JSON.parse('{"pO":[{"title":"Work","link":"work"},{"title":"Services","link":"services"},{"title":"Industries","link":"industries"},{"title":"Insights","link":"insights"},{"title":"Culture","link":"culture"},{"title":"Careers","link":"careers"}],"k1":[{"title":"Global","locale":"en","link":"/en"},{"title":"Nederland","locale":"nl","link":"/"},{"title":"United States","locale":"","link":"/us"},{"title":"Ireland","locale":"ie","link":"/us"},{"title":"United Kingdon","locale":"uk","link":"/uk"},{"title":"Deutschlandn","locale":"de","link":"/de"},{"title":"Schweiz","locale":"ch","link":"/ch"}]}');
// EXTERNAL MODULE: external "prop-types"
var external_prop_types_ = __webpack_require__(4229);
var external_prop_types_default = /*#__PURE__*/__webpack_require__.n(external_prop_types_);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./src/assets/icons/icon-menu.js


const MenuIcon = () => /*#__PURE__*/jsx_runtime_.jsx("svg", {
  width: "20",
  height: "9",
  xmlns: "http://www.w3.org/2000/svg",
  "aria-hidden": "true",
  focusable: "false",
  children: /*#__PURE__*/jsx_runtime_.jsx("g", {
    fillRule: "evenodd",
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M0 7h20v2H0zM0 0h20v2H0z"
    })
  })
});

/* harmony default export */ const icon_menu = (MenuIcon);
;// CONCATENATED MODULE: ./src/assets/icons/icon-close.js



const CloseIcon = () => /*#__PURE__*/jsx_runtime_.jsx("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 16 16",
  width: "16",
  height: "16",
  "aria-hidden": "true",
  focusable: "false",
  fill: "currentColor",
  children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("g", {
    fill: "currentColot",
    fillRule: "evenodd",
    children: [/*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M1.636 0l14.142 14.142-1.414 1.414L.222 1.414z"
    }), /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M.222 14.364L14.364.222l1.414 1.414L1.636 15.778z"
    })]
  })
});

/* harmony default export */ const icon_close = (CloseIcon);
// EXTERNAL MODULE: external "classnames"
var external_classnames_ = __webpack_require__(4058);
var external_classnames_default = /*#__PURE__*/__webpack_require__.n(external_classnames_);
// EXTERNAL MODULE: ./src/components/atoms/Links/AnchorTag.js
var AnchorTag = __webpack_require__(8478);
// EXTERNAL MODULE: ./src/assets/icons/icon-dropdown.js
var icon_dropdown = __webpack_require__(6464);
// EXTERNAL MODULE: ./src/components/molecules/Menus/Navigation.module.scss
var Navigation_module = __webpack_require__(6200);
var Navigation_module_default = /*#__PURE__*/__webpack_require__.n(Navigation_module);
;// CONCATENATED MODULE: ./src/components/molecules/Menus/Navigation.js





/**
 * Navigation component
 */




const Navigation = ({
  menuTitle = '',
  showTitle = false,
  id = '',
  isLarge = false,
  hasBorded = false,
  children,
  customClass = ''
}) => {
  const classes = external_classnames_default()((Navigation_module_default()).nav, customClass, {
    [(Navigation_module_default())["nav--large"]]: isLarge,
    [(Navigation_module_default())["nav--small"]]: !isLarge,
    [(Navigation_module_default())["nav--border"]]: hasBorded
  });
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("nav", {
    className: classes,
    "aria-labelledby": id,
    children: [/*#__PURE__*/jsx_runtime_.jsx("h2", {
      id: id,
      className: `info-text ${showTitle ? '' : 'screenreader'}`,
      children: menuTitle
    }), /*#__PURE__*/jsx_runtime_.jsx("ol", {
      className: (Navigation_module_default()).nav__list,
      children: children
    })]
  });
};

const Item = ({
  title = '',
  link = '/',
  isActive = false
}) => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("li", {
    "data-active": isActive,
    children: [isActive && /*#__PURE__*/jsx_runtime_.jsx("span", {
      className: (Navigation_module_default()).nav__icon,
      children: /*#__PURE__*/jsx_runtime_.jsx(icon_dropdown/* default */.Z, {})
    }), /*#__PURE__*/jsx_runtime_.jsx(AnchorTag/* default */.Z, {
      href: link,
      children: title
    })]
  }, title);
};

Navigation.Item = Item;
const {
  string,
  bool,
  node
} = (external_prop_types_default());
/* harmony default export */ const Menus_Navigation = (Navigation);
// EXTERNAL MODULE: ./src/components/molecules/Menus/Main.module.scss
var Main_module = __webpack_require__(3365);
var Main_module_default = /*#__PURE__*/__webpack_require__.n(Main_module);
;// CONCATENATED MODULE: ./src/components/molecules/Menus/Main.js











const MainMenu = ({
  isExpanded,
  toggleMenu
}) => {
  const closeButton = (0,external_react_.useRef)();
  const openButton = (0,external_react_.useRef)();
  const {
    pathname
  } = (0,router_namespaceObject.useRouter)(); // Switch focus between menu and close button

  (0,external_react_.useEffect)(() => {
    isExpanded ? closeButton.current.focus() : openButton.current.focus();
  }, [isExpanded]);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("button", {
      onClick: toggleMenu,
      "aria-expanded": isExpanded,
      "aria-controls": "menu-list",
      hidden: isExpanded,
      ref: openButton,
      className: (Main_module_default()).button__menu,
      children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
        children: "Menu"
      }), /*#__PURE__*/jsx_runtime_.jsx(icon_menu, {})]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: (Main_module_default())["menu--expanded"],
      hidden: !isExpanded,
      children: [/*#__PURE__*/jsx_runtime_.jsx("button", {
        onClick: toggleMenu,
        type: "button",
        id: "close",
        "aria-label": "Close menu",
        ref: closeButton,
        className: (Main_module_default()).button__close,
        children: /*#__PURE__*/jsx_runtime_.jsx(icon_close, {})
      }), /*#__PURE__*/jsx_runtime_.jsx(Menus_Navigation, {
        id: "language-nav",
        menuTitle: "Landen",
        showTitle: true,
        customClass: (Main_module_default()).menu__languages,
        children: menuItems_namespaceObject.k1.map(item => /*#__PURE__*/jsx_runtime_.jsx(Menus_Navigation.Item, {
          title: item.title,
          link: item.link,
          isActive: pathname.includes(item.link)
        }, item.title))
      }), /*#__PURE__*/jsx_runtime_.jsx(Menus_Navigation, {
        id: "primary-nav",
        menuTitle: "Primary navigation",
        isLarge: true,
        hasBorded: true,
        customClass: (Main_module_default()).menu__primary,
        children: menuItems_namespaceObject.pO.map(item => /*#__PURE__*/jsx_runtime_.jsx(Menus_Navigation.Item, {
          title: item.title,
          link: item.link,
          isActive: pathname.includes(item.link)
        }, item.link))
      })]
    })]
  });
};

const {
  func,
  bool: Main_bool
} = (external_prop_types_default());
/* harmony default export */ const Main = (MainMenu);
;// CONCATENATED MODULE: ./src/assets/icons/icon-dept.js


const DeptLogo = () => /*#__PURE__*/jsx_runtime_.jsx("svg", {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 173 46",
  fill: "currentColor",
  children: /*#__PURE__*/jsx_runtime_.jsx("path", {
    d: "M147.9 46h12.6V10.8H173V0h-37.6v10.8h12.5V46zm-41.5-24.9V9.9h7.2c4.6 0 6.9 1.9 6.9 5.6 0 3.7-2.3 5.6-6.9 5.6h-7.2zM93.9 46h12.6V31h7.5c11.3 0 18.8-4.9 18.8-15.5S125.2 0 114 0H93.9v46zm-43.3 0h34.3V35.7H63.2v-7.8h21.5V18H63.2v-7.7h21.7V0H50.6v46zm-38-10.7V10.7h4c8.8 0 14.3 3.2 14.3 12.3 0 9.1-5.4 12.3-14.3 12.3h-4zM0 46h16.5c15.6 0 26.9-6.2 26.9-23C43.3 6.2 32 0 16.5 0H0v46z",
    fill: "currentColor"
  })
});

/* harmony default export */ const icon_dept = (DeptLogo);
// EXTERNAL MODULE: ./src/components/atoms/Images/Image.js + 1 modules
var Image = __webpack_require__(8334);
// EXTERNAL MODULE: ./src/components/molecules/Header/Header.module.scss
var Header_module = __webpack_require__(6138);
var Header_module_default = /*#__PURE__*/__webpack_require__.n(Header_module);
;// CONCATENATED MODULE: ./src/components/molecules/Header/Header.js
/* eslint-disable react-hooks/exhaustive-deps */








/** Main header containing logo, hero and menu */



function Header() {
  const {
    0: isExpanded,
    1: setIsExpanded
  } = (0,external_react_.useState)(false);
  const {
    pathname
  } = (0,router_namespaceObject.useRouter)();
  const {
    0: pageTitle,
    1: setPageTitle
  } = (0,external_react_.useState)(''); // Close menu when menu item is selected

  (0,external_react_.useEffect)(() => {
    var _menuItems$menuItems$;

    if (isExpanded) toggleMenu();
    console.log();
    setPageTitle((_menuItems$menuItems$ = menuItems_namespaceObject.pO.find(i => pathname.includes(i.link))) === null || _menuItems$menuItems$ === void 0 ? void 0 : _menuItems$menuItems$.link);
  }, [pathname]); // Make sure the escape key closes the menu

  (0,external_react_.useEffect)(() => {
    document.addEventListener('keydown', escFunction);
    return () => {
      document.removeEventListener('keydown', escFunction);
    };
  }, []);
  const escFunction = (0,external_react_.useCallback)(event => {
    if (event.keyCode === 27 && isExpanded) {
      toggleMenu();
    }
  }, []);

  const toggleMenu = () => {
    setIsExpanded(!isExpanded);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("header", {
    className: (Header_module_default()).header,
    "data-is-expanded": isExpanded,
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: (Header_module_default()).nav,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (Header_module_default()).logo,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(AnchorTag/* default */.Z, {
          href: "/",
          children: [/*#__PURE__*/jsx_runtime_.jsx(icon_dept, {}), /*#__PURE__*/jsx_runtime_.jsx("span", {
            className: "screenreader",
            children: "Home"
          })]
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(Main, {
        isExpanded: isExpanded,
        toggleMenu: toggleMenu
      })]
    }), !isExpanded && /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: (Header_module_default()).hero,
      children: [/*#__PURE__*/jsx_runtime_.jsx(Image/* default */.Z, {
        src: `images/Header.webp`,
        alt: "",
        layout: "responsive"
      }), /*#__PURE__*/jsx_runtime_.jsx("h1", {
        className: (Header_module_default()).hero__title,
        children: pageTitle
      })]
    })]
  });
}
// EXTERNAL MODULE: ./src/components/layouts/containerLayout.module.scss
var containerLayout_module = __webpack_require__(4199);
var containerLayout_module_default = /*#__PURE__*/__webpack_require__.n(containerLayout_module);
;// CONCATENATED MODULE: ./src/components/layouts/containerLayout.js





function containerLayout({
  children,
  siteData
}) {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: (containerLayout_module_default()).container,
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)((head_default()), {
      children: [/*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "icon",
        href: "/favicon.ico"
      }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
        name: "author",
        content: siteData.author
      }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
        name: "description",
        content: siteData.description
      }), /*#__PURE__*/jsx_runtime_.jsx("title", {
        children: siteData.title
      }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
        property: "og:title",
        content: siteData.title
      }, "title")]
    }), /*#__PURE__*/jsx_runtime_.jsx(Header, {}), /*#__PURE__*/jsx_runtime_.jsx("main", {
      className: (containerLayout_module_default()).main,
      children: children
    })]
  });
}
;// CONCATENATED MODULE: ./pages/_app.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






function MyApp({
  Component,
  pageProps
}) {
  return /*#__PURE__*/jsx_runtime_.jsx(containerLayout, {
    siteData: siteconfig,
    children: /*#__PURE__*/jsx_runtime_.jsx(Component, _objectSpread({}, pageProps))
  });
}

/* harmony default export */ const _app = (MyApp);

/***/ }),

/***/ 4199:
/***/ ((module) => {

// Exports
module.exports = {
	"container": "containerLayout_container__3CSBO",
	"main": "containerLayout_main__2JVKX"
};


/***/ }),

/***/ 6138:
/***/ ((module) => {

// Exports
module.exports = {
	"header": "Header_header__3vjs8",
	"nav": "Header_nav__1TbvN",
	"hero": "Header_hero__1X4tL",
	"hero__title": "Header_hero__title__2lL1q",
	"logo": "Header_logo__3PlL6"
};


/***/ }),

/***/ 3365:
/***/ ((module) => {

// Exports
module.exports = {
	"menu--expanded": "Main_menu--expanded__1XbIL",
	"menu__primary": "Main_menu__primary__76o1G",
	"menu__languages": "Main_menu__languages__33pVs",
	"menu--expanded__languages": "Main_menu--expanded__languages__2Sklk",
	"button__menu": "Main_button__menu__2IzZ8",
	"button__close": "Main_button__close__3m1ip"
};


/***/ }),

/***/ 6200:
/***/ ((module) => {

// Exports
module.exports = {
	"nav": "Navigation_nav__1sKsa",
	"nav--large": "Navigation_nav--large__4QI5o",
	"nav--small": "Navigation_nav--small__1q-QQ",
	"nav__list": "Navigation_nav__list__3WyEL",
	"nav--border": "Navigation_nav--border__atbWh",
	"nav__icon": "Navigation_nav__icon__1tsAK"
};


/***/ }),

/***/ 4058:
/***/ ((module) => {

"use strict";
module.exports = require("classnames");

/***/ }),

/***/ 9325:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 822:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 6695:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 8300:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/detect-domain-locale.js");

/***/ }),

/***/ 5378:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 7162:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 8773:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 2248:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 9372:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 665:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 2747:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 333:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 3456:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 556:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 7620:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 4229:
/***/ ((module) => {

"use strict";
module.exports = require("prop-types");

/***/ }),

/***/ 9297:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 5282:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [664,675,519,921], () => (__webpack_exec__(2391)));
module.exports = __webpack_exports__;

})();